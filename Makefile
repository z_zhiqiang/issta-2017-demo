MAIN_FILE = paper

all: $(MAIN_FILE)

$(MAIN_FILE):
	latex $(MAIN_FILE)
	dvips -o $(MAIN_FILE).ps -Pdownload35 -t letter $(MAIN_FILE)

old:
	latex $(MAIN_FILE)
	bibtex $(MAIN_FILE)
	latex $(MAIN_FILE)
	latex $(MAIN_FILE)
	dvips -o $(MAIN_FILE).ps -Pdownload35 -t letter $(MAIN_FILE)
	ps2pdf ${MAIN_FILE}.ps
full:
	ps2pdf -sPAPERSIZE=letter \
		-dMaxSubsetPct=100 -dCompatibilityLevel=1.3 \
		-dSubsetFonts=true -dEmbedAllFonts=true \
		-dAutoFilterColorImages=false \
		-dAutoFilterGrayImages=false \
		-dColorImageFilter=/FlateEncode \
		-dGrayImageFilter=/FlateEncode \
		-dModoImageFilter=/FlateEncode \
		$(MAIN_FILE).ps $(MAIN_FILE).pdf


pdf:
	pdflatex $(MAIN_FILE)
	bibtex $(MAIN_FILE)
	pdflatex $(MAIN_FILE)
	pdflatex $(MAIN_FILE)


clean:
	@rm -f *~ $(MAIN_FILE).dvi $(MAIN_FILE).pdf $(MAIN_FILE).ps $(MAIN_FILE).toc *.log *.tmp *.bak *.aux *.bbl *.blg *.idx *.ind *.ilg *.gls *.glo *.lof *.lot *.out core *.dvi

view: 
	cp $(MAIN_FILE).pdf ~/public_html/graspan_demo.pdf