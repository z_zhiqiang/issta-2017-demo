\section{CFL Reachability Formulation}\label{sec:formulation}

%\subsection{Graph Reachability} 
It has been shown that many static program analyses can be formulated as CFL reachability problems \cite{reps-ist98}.
Pioneered by Reps et al.~\cite{reps-popl95, sagiv-tcs96}, there is a
large body of work on graph reachability based program
analyses~\cite{kodumal-pldi04, xu-ecoop09, yan-issta11, rehof-popl01,
  bastani-popl15, zhang-oopsla14, zhang-pldi13, tang-popl15}. The
reachability computation is often guided by a context-free grammar.
Here we take pointer/alias analysis as an example to show how it can be formulated as a graph reachability problem.

%\subsection{Pointer Analysis} 

A pointer analysis computes, for each pointer variable, a set of heap objects
(represented by allocation sites) that can flow to the variable. This
set of objects is referred to as the variable's \emph{points-to} set.
Alias information can be derived from this analysis --- if the
points-to sets of two variables have a non-empty intersection, they
may alias.
Our graph formulation of pointer analysis is adapted from a previous
formulation in~\cite{zheng-popl08}. 


\MyPara{Pointer Analysis as Graph Reachability} For simplicity of
presentation, the discussion here focuses on four kinds of three-address
statements (which are statements that have at most three operands):\\

\begin{tabular}{cr|cr}
$a~=~b$ & Value assignment & $a~=~*b$ & Load \\\hline
$*b~=~a$  & Store & $a~=~\&b$ & Address-of \\
\end{tabular}\\

Complicated statements are often broken down into these three-address
statements in the compilation process by introducing temporary
variables. 

For each function, an \emph{expression graph} -- whose vertices
represent C expressions and edges represent value flow between
expressions --- is generated; graphs for different functions are
eventually connected to form a whole-program expression graph. Each
vertex on the graph represents an expression, and each edge is of
three kinds:

\begin{itemize}
%\addtolength{\itemsep}{-0.5\baselineskip}
%\vspace*{-0.5em}
\item \emph{Dereference edge (D)}: for each dereference $*a$, there is
  a D-edge from $a$ to $*a$; there is also an edge from an address-of
  expression $\&a$ to $a$ because $a$ is a dereference of $\&a$.
\item \emph{Assignment edge (A)}: for each assignment $a~=~b$, there
  is an A-edge from $b$ to $a$; $a$ and $b$ can be arbitrary
  expressions.
\item \emph{Alloc edge (M)}: for each assignment $a~=~\mathit{malloc}()$, 
there is an M-edge from a special Alloc vertex to $a$.
\end{itemize}

\begin{figure}[t]
\centering
\includegraphics[scale=0.7]{figs/peg}
%\vspace{-2em}
\caption{A program and its expression graph: solid, horizontal
  edges represent assignments (A- and M- edges); dashed, vertical edges
  represent dereferences (D-edge); dotted, horizontal edges represent transitive edges labeled non-terminals. $\mathit{A4}$ indicates the allocation site at
  Line 4. %$\mathit{OF}$, $\mathit{VF}$, and $\mathit{AL}$ represent \textit{objectFlow}, \textit{valueFlow}, and \textit{alias}, respectively.
\label{fig:peg}}
\vspace{-.5em}
\end{figure}

Figure~\ref{fig:peg} shows a simple program and its expression
graph. Each edge has a label, indicating its type. Solid and dashed
edges are original edges in the graph and they are labeled $M$, $A$,
or $D$, respectively. Dotted edges are transitive edges\footnote{We
  use term ``transitive edges'' to refer to the edges
  dynamically added to represent non-terminals rather than
the transitivity of a relation.} added by Graspan into the graph, as
discussed shortly.

\MyPara{Context-free Grammar} The pointer information computation is
guided by the following grammar:

\begin{tabbing}
Object flow: \hspace{1em} \=\textit{OF} \hspace{1.5em}\=::=~~~$M$~~$\mathit{VF}$\\
Value flow: \> $\mathit{VF}$\>::=~~~$(A~~\mathit{MA}?)^{\ast}$\\
Memory alias: \> \textit{MA} \>::=~~~$\overline{D}~~\mathit{VA}~~D$\\
Value alias: \> \textit{VA} \> ::=~~~$\overline{VF}~~\mathit{MA}?~~\mathit{VF}$ %\\[-1.5em]
\end{tabbing}

This grammar has four non-terminals \textit{OF}, \textit{VF},
\textit{MA}, and \textit{VA}. For a non-terminal $T$, a path in the
graph is called a $T$-path if the sequence of the edge labels on the
path is a string that can be reduced to $T$. In order for a variable
$v$ to point to an object $o$ (\ie, a malloc), there must exist an
\textit{OF} path in the expression graph from $o$ to $v$.  The
definition of $\mathit{OF}$ is straightforward: it must start with an
alloc ($M$) edge, followed by a $\mathit{VF}$ path that propagates the
object address to a variable. A $\mathit{VF}$ path is either a sequence
of simple assignment (A) edges or a mix of assignments edges and
\textit{MA} (memory alias) paths.

There are two kinds of aliasing relationships in C: memory aliasing
(MA) and value aliasing (VA).  Two lvalue expressions are memory
aliases if they may denote the same memory location while they are
value aliases if they may evaluate to the same value.



\MyPara{Example} In Figure~\ref{fig:peg}, $e$ points to $\mathit{A4}$,
since the $M$ edge between them forms an \textit{OF} path. There is a
$\mathit{VF}$ path from $\&a$ to $d$, which is also a $\mathit{VA}$
path (since $\mathit{VA}$ includes $\mathit{VF}$). The $\mathit{VA}$
path enables an $\mathit{MA}$ path from $a$ to $*d$ due to the
balanced parentheses $D$ and $\overline{D}$. This path then induces
two additional $\mathit{VF}$/$\mathit{VA}$ paths from $b$ to $t$ and
from $\&c$ to $t$, which, in turn, contribute to the forming of the
$\mathit{VF}/\mathit{VA}$ path from $c$ to $x$, making $*c$ and $*x$
memory aliases. Hence, there exists a $\mathit{VF}$ path from $e$ to
$y$, which, together with the $M$ edge at the beginning, forms an
$\mathit{OF}$ path from $\mathit{A4}$ to $y$. This path
indicates that $y$ points to $\mathit{A4}$.  The dotted edges in
Figure~\ref{fig:peg} show these transitive edges.

\MyPara{Traditional Solution} The traditional way to implement this
analysis is to maintain a worklist, each element of which is a pair of
a newly discovered vertex and a stack simulating a pushdown
automaton. The implementation loops over the worklist, iteratively
retrieving vertices and processing their edges. The traditional
implementation does not add any physical edges into the graph (due to
the fear of memory blowup), but instead, it tracks path information
using pushdown automata. When a CFL-reachable vertex is detected, the
vertex is pushed into the worklist together with the sequence of the
labels on the path leading to the vertex. When the vertex is popped
off of the list, the information regarding the reachability from the
source to the vertex is discarded.

This traditional approach has at least two significant
drawbacks. First, it does not scale well when the analysis becomes
more sophisticated or the program to be analyzed becomes larger. For
example, when the analysis is made \emph{context-sensitive}, the
grammar needs to be augmented with the parentheses representing method
entries/exists; the checking of the balanced property for these
parentheses also needs to performed. Since the number of distinct
calling contexts can be very large for real-world programs, \naively
traversing all paths is guaranteed to be not scalable in practice. As
a result, various abstractions and tradeoffs~\cite{sridharan-pldi06,
  smaragdakis-popl11, kastrinis-pldi13, smaragdakis-pldi14} have been
employed, attempting to improve scalability at the cost of precision
as well as implementation straightforwardness.

Second, the worklist-based model is notoriously difficult to
parallelize, making it hard to fully utilize modern computing
resources. Even if multiple traversals can be launched simultaneously,
since none of these traversals add transitive edges into the program
graph as they are being detected, every traversal performs path discovery
completely independently, resulting in a great deal of wasted efforts.

\MyPara{A ``Big Data'' Perspective} Our key insight here is that
adding \emph{physical} transitive edges into the program graph makes
it possible to devise a Big Data solution to this static analysis
problem for two reasons. First, representing transitive edges
\emph{explicitly} rather than \emph{implicitly} leads to addition of a
great number of edges (\eg, even larger than the number of edges in
the original graph). This gives us a large (evolving) dataset to
process.  Second, the computation only needs to match the labels of
consecutive edges with the productions in the grammar and is thus
simple enough to be ``systemized''.  Of course, dynamically adding
many edges can make the computation quickly exhaust the main
memory. However, this should not be a concern, as there are already
many systems~\cite{graphlab, graphchi, graphq, roy-sosp15, graphx,
  vora-atc16} built to process very large graphs (\eg, the webgraph
for the whole Internet).






%\subsection{Dataflow Analysis}
%
%Following Rep et al.'s interprocedural,
%finite, distributive, subset (IFDS) framework~\cite{reps-popl95}, we
%have also formulated a fully context-sensitive dataflow analysis as a
%grammar-guided reachability problem. Specifically, we use this
%dataflow analysis to track NULL value propagation.  Due to space
%limitations, the discussion of the formulation is omitted from the
%paper.

%One slight difference between the IFDS framework and our
%formulation is that we achieve context sensitivity also by cloning
%intraprocedural graphs instead of using the summary-based approach
%in~\cite{reps-popl95}, which has been
%demonstrated~\cite{whally-pldi04} to fall short in answering many user
%queries.


