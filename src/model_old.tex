\section{Graspan's Programming Model}\label{sec:model}
In this section, we describe Graspan's programming model, \ie, the
tasks that need to be done by the programmer to use Graspan. There
are two main tasks. The first task is to modify a compiler frontend
to generate the graph. The second task is to use the Graspan API to
specify the grammar. Next, we will elaborate on these two tasks.
We will then finish the section by discussing the applicability of Graspan's programming model to interprocedural analyses.

\MyPara{Generating Graph}
For Graspan to perform an interprocedural analysis, the user first
needs to generate the {\em Graspan graph}, which is a specialized
program graph tailored for the analysis, by modifying a compiler
frontend. Note that since this task is relatively simple, the
developer can generate the Graspan graph in a mechanical way without
even thinking about performance and scalability. In this subsection,
we briefly discuss how we generate the Graspan graph in the context of
the pointer/alias analysis. We finish by generalizing graph generation
for other interprocedural analyses.


%\ardalan{Here, can we add a paragraph explaining the generic properties of the Graspan graph? It seems to me that these properties are (1) inclusion of inverse edges, and (2) context sensitivity achieved through inlining. Is this correct? Are there any other properties? I'd like to summarize these here and then explain them in the following paragraph.}
%\harry{these are very specific to the analysis being performed. You can summarize them here : )}
%\ardalan{I did. But I still think it's important that we give a high-level view of Graspan's graph regardless of the analysis. Are there any common properties? Note that you finish the last paragraph saying that graph for other interprocedural analysis can be generated in a similar manner. What is "a similar manner?" That's what I'm trying to get at.}
%\harry{add a paragraph to summarize them at the end of this subsection.}

For the pointer/alias analysis, we generate the Graspan graph by
making two modifications to the program expression graph described in
\S\ref{sec:background}. These modifications include (1) inclusion of
inverse edges and (2) context sensitivity achieved through
inlining. For the former, we model inverse edges explicitly. That is,
for each edge from $a$ to $b$ labeled $X$, we create and add to the
graph an edge from $b$ to $a$ labeled $\overline{X}$.

For the latter, we perform a bottom-up (\ie, reverse-topological)
traversal of the call graph of the program to inline functions. For
each function, we make a \emph{clone} of its entire expression graph
for each call site that invokes the function. Formal and actual
parameters are connected explicitly with edges. The cloning of a graph
not only copies the edges and vertices in one function; it does so for
\emph{all} edges and vertices in its (direct and transitive) callees.

For recursive functions, we follow the standard
treatment~\cite{whaley-pldi04} -- strongly connected components (SCC)
are computed and then functions in each SCC are collapsed into one
single function, and treated context insensitively. Clearly, the size
of the graph grows exponentially as we make clones and the generated
graph is often large. However, the out-of-core support in Graspan
guarantees that Graspan can analyze even such large graphs
effectively.  For each copy of a vertex, we generate a unique ID in a
way so that we can easily locate the variable its corresponds to and
its containing function from the ID.  In the Graspan graph, edges
carry data (\ie, their labels) but vertices do not. Finally, the graph
is dumped to disk in the form of an edge list.

In general, the approach of aggressive inlining provides
\emph{complete information} that an analysis intends to uncover.
Among all the existing analysis implementations, only Whaley et
al.~\cite{whaley-pldi04} could handle such aggressive inlining but
they only clone variables (\emph{not} objects) and have to use a
binary decision diagram (BDD) to merge results. In addition, no
evidence was shown that their analysis could process the Linux
kernel. On the contrary, Graspan processes the exploded kernel graph
in a few hours on a single machine.

Although this subsection focuses on the generation of pointer analysis
graphs, graphs for other analyses can be generated in a similar
manner. Here we briefly summarize the steps. First, vertices and edges
need to be defined based on a grammar; this step is analysis-specific.
Second, if inverse edges are needed in the grammar, they need to be
explicitly added. Finally, context sensitivity can be generally
achieved by function inlining. The developer can easily control the
degree of context sensitivity by using different inlining
criteria. For example, we perform \emph{full context sensitivity} and
thus our inlining goes from the bottom functions all the way up the top
functions of the call graph. But if one wishes to perform only
\emph{one-level} context sensitivity, each function only needs to be
inlined once.

\MyPara{Specifying Grammar}
Once the program graph is generated, the user needs to specify a
grammar that guides the addition of transitive edges at run time.
Unlike any traditional implementation of the analysis, Graspan adds
transitive edges (\eg, dotted edges in Figure~\ref{fig:peg}) to the
graph in a parallel manner. Specifically, for each production in the
grammar, if Graspan finds a path whose edge labels match the RHS terms
of the production, a transitive edge is added covering the path and
labeled with the LHS of the production.

Since Graspan uses the edge-pair-centric model, it focuses on a pair
of edges at a time, which requires each production in the grammar to
have no more than two terms on its RHS. In other words, the length of
a path Graspan checks at a time must be $\leq$ 2.

For example, the above mentioned pointer analysis grammar cannot be
directly used, because the RHSes of \textit{VF}, \textit{MA}, and
\textit{VA} all have more than two terms. This means that to add a new
\textit{VF} edge, we may need to check more than two consecutive
edges, which does not fit into Graspan's EP-centric
model. Fortunately, every context free grammar can be
\emph{normalized} into an equivalent grammar with at most two terms on
its RHS~\cite{reps-popl95}, similar to the Chomsky normal form.  After
normalization, our pointer analysis grammar becomes:


\begin{tabbing}
Object flow: \hspace{1.5em}
\=\textit{OF} \hspace{1.5em}\=::=~~~\=$M$~~$\mathit{VF}$\\ Temp:
\> $T_1$ \>::=\>$A~~|~~\mathit{MA}$\\ Value flow: \>
$\mathit{VF}$\>::=\>$T_1~~|~~\mathit{VF}~~\mathit{T_1} |~~\epsilon$
\\ Mem alias: \> \textit{MA} \>::=\>$T_2~~D$\\ Temp:\> $T_2$ \>::= \>
$\overline{D}~~\mathit{VA}$ \\ Value alias: \> \textit{VA}
\>::=\>$T_3~~\mathit{VF}$ \\ 
Temp: \>$T_3$ \>::= \> $\overline{VF}~~\mathit{MA}~~|~~\overline{VF}$\\[-1.5em]
\end{tabbing}

At the center of Graspan's programming model is an API  \\
\noindent \texttt{addConstraint}(\texttt{Label} \textit{lhs}, \texttt{Label} \textit{rhs1}, \texttt{Label} \textit{rhs2}), \\
\noindent which can be used by the developer to register each
production in the grammar. \textit{lhs} represents the LHS
non-terminal while \textit{rhs1} and \textit{rhs2} represent the two
RHS terms. If the RHS has only one term,
\textit{rhs2} should be NULL. 

\MyPara{Graspan Applicability}
How many interprocedural analyses can be powered by Graspan? First, we
note that pointer analysis and dataflow analysis are already
representatives of a large number of analysis algorithms that can be
formulated as a grammar-guided graph reachability problem. Second,
work has been done to establish the convertibility from other types of
analysis formulation (\eg, set-constraint~\cite{kodumal-pldi04} and
pushdown systems~\cite{alur-toplas05, alur-toplas05, alur-pods07}) to
context-free language reachability. Analyses under these other
formulations can all be parallelized and made scalable by Graspan.

Note that Graspan currently does not support analyses that require
constraint solving, such as path-sensitive analysis and symbolic
execution.  In our future work, we plan to add support for
constraint-based analyses by encoding constraints into edge
values. Two edges match if a satisfiable solution can be found for the
conjunction of the constraints they carry.

