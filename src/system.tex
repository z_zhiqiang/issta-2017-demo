\section{Graspan system}\label{sec:system}
\todo{Couple sentences to describe the section}

\subsection{Preprocessing} 
Preprocessing partitions the Graspan graph generated for an
analysis. The graph is in the edge-list format on disk. Similar to
graph sharding in GraphChi~\cite{graphchi}, partitioning in Graspan is
done by first dividing vertices into \emph{logical
intervals}. However, unlike GraphChi that groups edges based on their
target vertices, one interval in Graspan defines a partition that
contains edges whose \emph{source vertices} fall into the
interval. Edges are sorted on their source vertex IDs and those that
have the same source are stored consecutively and \emph{ordered on
their target vertex IDs}. The fact that the outgoing edges for each
vertex are sorted enables quick edge addition, as we will discuss
shortly.

Graspan loads two partitions at a time and joins their edge-lists, a
process we refer to as a \emph{superstep}.  Given that only two
partitions reside in memory at a given time, the size and hence the
total number of partitions are determined automatically by the amount
of memory available to Graspan.

Preprocessing also produces three pieces of meta-information: a
\emph{degree file} for each partition, which records the (incoming and
outgoing) degrees of its vertices, a global \emph{vertex-interval
table} (VIT), which specifies vertex intervals, and a
\emph{destination distribution map} (DDM) for each partition $p$ that
maps, for each other partition $q$, the percentage of the edges in $p$
that go into $q$. The DDM is essentially a matrix, each cell
containing a percentage.

Graspan uses the degree file to calculate the size of the array to be
created to load a partition. Without the degree information, a
variable-size data structure (\eg, ArrayList) has to be used, which
would incur array resizing and data copying operations. The
VIT records the lower and upper-bounds for each interval (\eg, (0, [0,
10000]), (1, [10001, 23451]), \etc). Graspan maintains the table
because the intervals will be redefined upon repartitioning. The DDM
measures the ``matching'' degree between two partitions and will be
used by the Graspan scheduler to determine which two to load.

\subsection{Edge-Pair Centric Computation\label{sec:compute}}
Graspan supports in-memory and out-of-core computations. For small
graphs that can be held in memory, our preprocessing only generates
two partitions, both of which are resident in memory.  For large
graphs with more than two partitions, Graspan uses a scheduling
algorithm (discussed shortly) to load two partitions in each
superstep, joins their edge lists, updates their edges, and performs
repartitioning if necessary. Each superstep itself performs a fixed
point computation --- newly added edges may give rise to further
edges.

The computation is finished when no new edges can be added. The
updated edge lists may or may not be written back to disk depending on
the next two partitions selected by the scheduler. This iterative
process is repeated until a global fixed point is reached, that is, no
new edges can be added for any pair of partitions.  Since the VIT and
the DDM are reasonably small in size, they are kept in memory
throughout the processing.

For the computation, Graspan reads two partitions into memory as a
list of vertices with its outgoing edges, merging them into one single
list which tracks old, ``old and new'', and only new edges
separately. With a single list, the computation can be easily split
into separate chunks and passed to multiple threads. At each vertex,
Graspan checks the outgoing edges to find potential new edges that can
be added according to the specified grammar. At this point, the
distinction between new and old edge lists is important because it
allows the program to only check through new edges, without having to
check the same edges multiple times. Graspan checks the new edges for
one-hop edges, then checks the old and new edges to find new two-hop
edges. At each outgoing vertex, it creates a list of possible edges to
add. Because the edges are stored in order sorted by vertex ID, each
list of potential edges to add also remains sorted, allowing
duplicates to be easily found during the merging process. The lists of
new potential edges is then merged by repeatedly finding the minimum
vertex ID to be added using a priority queue. This allows the program
to easily compare vertex ID`s and edge values with edges already
contained by the source, as well as other edges that are going to be
added. In order to maintain a consistent state of the graph during
computation and ensure that all edges are added correctly, the new
edges are appended to a ``delta'' list until such time that all
threads have finished computing. The new, old, and ``old and new''
edge lists are then updated appropriately.

\subsection{Postprocessing} 
When a superstep is done, the updated edge lists need to be written
back to their partition files.  In addition, the degree file is
updated with the new vertex degree information. The (in-memory) DDM
needs to be updated with the new edge distribution information.

\MyPara{Repartitioning} If the size of a partition exceeds a threshold
(\eg, a parameter), repartitioning occurs. It is easy for Graspan to
repartition an oversized partition since the edge lists are
sorted. Graspan breaks the original vertex interval into two small
intervals, and edges are automatically restructured. The goal is to
have the two small vertex intervals to have similar numbers of edges,
so that the resulting partitions have similar sizes. The VIT needs to
be updated with the new interval information. Repartitioning can also
be triggered in the middle of a superstep if too many edges are added
in the superstep and the size of the loaded partitions is close to the
memory size.

\MyPara{Scheduling} When a new superstep starts, two new partitions
will be selected by the scheduler to join. Since a
partition on which the computation was done in the previous superstep
may be chosen again, Graspan delays the writing of a partition back to
disk until the new partitions are chosen by the scheduler. If a chosen
partition is already in memory, significant amounts of disk I/O can be
saved.

\MyPara{Reporting Results} Graspan provides an API for the user to
iterate over edges with a certain label. For example, for the pointer
analysis, edges with the $\mathit{OF}$ label indicate a points-to
solution, while edges with the $\mathit{MA}$ and $\mathit{VA}$ label
represent aliasing variables. Graspan also provides translation APIs
that can be used to map vertices and edges back to variables and
statements in the program.
\todo{edges labels appear out of nowhere}
