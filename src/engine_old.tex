\section{Graspan System}\label{sec:engine}


\begin{figure*}[t]
\begin{tabular}{ccc}
    \begin{minipage}{0.22\linewidth}
%    \begin{subfigure}[b]{0.22\linewidth}
    \includegraphics[width=1.6in]{figs/graph}
%\vspace{-1.5em}
%    \caption{ \label{fig:graph}}
\end{minipage}
&
%    \end{subfigure}
 \hspace{.5em}
 \begin{minipage}{0.23\linewidth}
    \centering
    \includegraphics[width=1.8in]{figs/partitions}
%\vspace{-1.5em}
%    \caption{\label{fig:partition}}
%    \end{subfigure}
\end{minipage}
&
  \hspace{2em}
%    \begin{subfigure}[b]{0.45\linewidth}
\begin{minipage}{0.45\linewidth}
    \includegraphics[width=3in]{figs/edges}
%    \end{subfigure}
\end{minipage}\\
(a) & (b) & (c)
\end{tabular}
  \caption{(a) An example graph, (b) its partitions, and (c) the
  in-memory representation of edge lists. \label{fig:example}}
\vspace{-1.5em}
\end{figure*}

%\section{Graspan Design and Implementation\label{sec:engine}}
We implemented Graspan first in Java. Due to performance issues in the
JVM (most of which were caused by the GC when copying arrays), we
re-implemented Graspan in C++. The Java and C++ versions have an
approximate 6K and 4K lines of code, respectively.  Graspan can
analyze programs written in any languages.

\subsection{Preprocessing} 
Preprocessing partitions the Graspan graph generated for an analysis. The graph
is in the edge-list format on disk. Similar to graph sharding in
GraphChi~\cite{graphchi}, partitioning in Graspan is done by first
dividing vertices into \emph{logical intervals}. However, unlike
GraphChi that groups edges based on their target vertices, one interval
in Graspan defines a partition that contains edges whose \emph{source
  vertices} fall into the interval. Edges are sorted on their
source vertex IDs and those that have the same source are stored
consecutively and \emph{ordered on their target vertex IDs}. The fact
that the outgoing edges for each vertex are sorted enables quick edge
addition, as we will discuss shortly.
Figure~\ref{fig:example}(a) shows a simple directed graph. Suppose Graspan
splits its vertices into three intervals 0--2, 3--4, and 5--6;
Figure~\ref{fig:example}(b) shows the partition layout.

When a new edge is found during processing, it is always added to the
partition to which the source of the edge belongs. 
Graspan loads two partitions at a time and joins their edge-lists
(\S\ref{sec:compute}), a process we refer to as a \emph{superstep}.
Given that only two partitions reside in memory at a given
time, the size and hence the total number of partitions are determined
automatically by the amount of memory available to Graspan.

%In order to maximize
%memory utilization, we want these partitions (and some auxiliary data
%structures) to fill up the available memory (\ie, the Java maximum
%heap size specified by the user). Assuming the amount of available memory to be $M$,
%the following formula is then used to calculate the size of each
%partition: $s~=~\frac{M}{2 \times 1.2 \times 2}$, where $1.2$ ($20\%$)
%is the estimated overhead incurred by (object-oriented) data
%structures and the factor of $2$ leaves extra space to accommodate
%dynamically added edges. The number of partitions can be easily
%computed based on $s$ and the size of the graph.

Preprocessing also produces three pieces of meta-information: a
\emph{degree file} for each partition, which records the (incoming and
outgoing) degrees of its vertices, a global \emph{vertex-interval
  table} (VIT), which specifies vertex intervals, and a
\emph{destination distribution map} (DDM) for each partition $p$ that
maps, for each other partition $q$, the percentage of the edges in $p$
that go into $q$. The DDM is essentially a matrix, each cell
containing a percentage.

Graspan uses the degree file to calculate the size of the array to be
created to load a partition. Without the degree information, a
variable-size data structure (\eg, ArrayList) has to be used, which
would incur array resizing and data copying operations. The
VIT records the lower and upper-bounds for each interval (\eg, (0, [0,
  10000]), (1, [10001, 23451]), \etc). Graspan maintains the table
because the intervals will be redefined upon repartitioning. The DDM
measures the ``matching'' degree between two partitions and will be
used by the Graspan scheduler to determine which two to load.

\subsection{Edge-Pair Centric Computation\label{sec:compute}} 
Graspan supports in-memory and out-of-core computations. For small
graphs that can be held in memory, our preprocessing only generates
two partitions, both of which are resident in memory.
For large graphs with more than two partitions,
Graspan uses a scheduling algorithm (discussed shortly) to load two
partitions in each superstep, joins their edge lists, updates their
edges, and performs repartitioning if necessary. Each superstep itself
performs a fixed point computation --- newly added edges may give rise
to further edges. 

The computation is finished when no new edges can be added. The updated
edge lists may or may not be written back to disk depending on the
next two partitions selected by the scheduler. This iterative process
is repeated until a global fixed point is reached, that is, no new
edges can be added for any pair of partitions.
Since the VIT and the DDM are reasonably small in size, they are
kept in memory throughout the processing.

\MyPara{In-Memory Edge Representation} The
edge list of a vertex $v$ is represented as two arrays of
(vertex, label) pairs, as shown in Figure~\ref{fig:example}(c). The first
array ($O_v$) contains ``old'' edges that have been inspected before and
the second ($D_v$) contains edges newly added in the current
iteration. The goal is to avoid repeatedly
matching edge pairs (discussed shortly).

\begin{algorithm}[t]
	\small
	\KwIn{Partition $p_1$, Partition $p_2$}
	\DontPrintSemicolon
	\BlankLine
        Combine the vertices of $p_1$ and $p_2$ into $V$ \label{l:mergepartition1}\;
        Combine the edge lists of $p_1$ and $p_2$ into $E$ \label{l:mergepartition2}\;
        \ForPar{each edge list $v: (e_1, e_2, \ldots, e_{n}) \in E$}{
          Set $\mathit{O_v}$ to $()$ \label{l:ov}\;
          Set $\mathit{D_v}$ to $(e_1, e_2, \ldots, e_{n})$ \label{l:dv}\;
       }
        \ForPar{each vertex $v: (O_v, D_v)$ whose $D_v$ is NOT empty}{ \label{l:loopstart}
            Array $\mathit{mergeResult} \leftarrow ()$ \label{l:firststart}\; 
            Let $V_1$ be the intersection of the target vertices of $O_v$ and $V$ \label{l:intersect1}\;
 \BlankLine            
            /*Merge $O_v$ with only $D_v$ of other vertices*/\;
            List $\mathit{listsToMerge} \leftarrow \{O_v\}$ \label{l:createlists}\;
            \ForEach{Vertex $v' \in V_1$}{
              Add $D_{v'}$ into $\mathit{listsToMerge}$\;
            } \label{l:createlistsend}
            /*Merge the sorted input lists into a new sorted list*/ \;
            $\mathit{mergeResult} \leftarrow$ \textsc{MatchAndMergeSortedArrays}($\mathit{listsToMerge}$) \label{l:firstend}\;
            \BlankLine 
            /*Merge $D_v$ with $O_v \cup D_v$ of other vertices*/ \label{l:secondstart}\;
            Let $V_2$ be the intersection of the target vertices of $D_v$ and $V$ \;
            $\mathit{listsToMerge} \leftarrow \{D_v, \mathit{mergeResult}\}$\;
            \ForEach{Vertex $v' \in V_2$}{
              Add $O_{v'}$ and $D_{v'}$ into $\mathit{listsToMerge}$\;
            }
            $\mathit{mergeResult} \leftarrow$ \textsc{MatchAndMergeSortedArrays}($\mathit{listsToMerge}$) \label{l:secondend}\;
         %  }
        %\ForPar{each vertex $v: (O_v, D_v)$}{
 \BlankLine
            /*Update $O_v$ and $D_v$*/\;
            $\mathit{listsToMerge} \leftarrow \{O_v, D_v\}$\;
            $O_v \leftarrow$ \textsc{MergeSortedArrays}($\mathit{listsToMerge}$) \label{l:finalmerge}\;
            $D_v \leftarrow \mathit{mergeResult} - O_v $\;
          }\label{l:loopend}
	\caption{The parallel EP-centric computation.\label{alg:ep}}
\end{algorithm}


\MyPara{Parallel Edge Addition} Algorithm~\ref{alg:ep} shows a
BSP-like algorithm for the parallel EP-centric computation.
With two partitions $p_1$ and $p_2$ loaded, we first merge them into
one single partition with combined edge lists
(Line~\ref{l:mergepartition1} -- \ref{l:mergepartition2}).  Initially,
for each vertex $v$, its two arrays $O_v$ and $D_v$ are set to empty
list and the original edge list of $v$, respectively (Line~\ref{l:ov}
and Line~\ref{l:dv}).  The loop between Line~\ref{l:loopstart} and
Line~\ref{l:loopend} creates a separate thread to process each vertex
$v$ and its edge list, computing transitive edges using a
fixed-point computation with two main components.

The first component (Line~\ref{l:firststart} -- \ref{l:firstend})
attempts to match each ``old'' edge in $O_v$ that goes to vertex $u$ with
each ``new'' edge of $u$ in $D_u$.  The second component
(Line~\ref{l:secondstart} -- \ref{l:secondend}) matches each ``new''
edge in $D_v$ with both ``old'' and ``new'' edges in $O_u$ and $D_u$ of
vertex $u$.  The idea is that we do not need to match an ``old'' edge
of $v$ with an ``old'' edge of $u$, because this work has been done in
a previous iteration. $O_v$ and $D_v$ are updated at the end of each
iteration.

An important question is how to perform edge matching. A
straightforward approach is that, for each edge $v\xrightarrow{L_1}u$,
we inspect each of $u$'s outgoing edges $u\xrightarrow{L_2}x$, and add
an edge $v\xrightarrow{K}x$ if a production $K ::= L_1~L_2$
exists. However, this simple approach suffers from significant
practical limitations. First, before the edge is added into $v$'s
list, we need to scan $v$'s outgoing edges one more time to check if
the same edge already exists. Checking and avoiding duplicates is very
important -- duplicates may cause the analysis either not to terminate
or to suffer from significant redundancies in both time and space.

Doing a linear scan of the existing edges is expensive -- it has an
O($|E|^2$) complexity to add edges for each vertex, where $|E|$ is the
total number of edges loaded.  An alternative is to implement an
``offline'' checking mechanism that removes duplicates when writing
updated partitions to disk.  While this approach eliminates the cost
of online checks, it may prevent the computation from terminating ---
if the same edge is repeatedly added, missing the online check would
make the loop at Line~\ref{l:loopstart} keep seeing new edges and run
indefinitely.

Our algorithm performs \emph{quick edge addition} and \emph{online
  duplicate checks}. Our key insight is that edge addition can be done
\emph{in batch} much more efficiently than \emph{individually}. To
illustrate, consider Figure~\ref{fig:example}(a) where vertex 0
initially has two outgoing edges $0\rightarrow 1$ and $0\rightarrow
4$.  Adding new edges for vertex 0 is essentially the same as
\emph{merging} the (outgoing) edges of vertex 1 and 4 into vertex 0's
edge list and then filtering out those that have mismatched labels.

In Algorithm~\ref{alg:ep}, to add edges for vertex $v$, we first
compute set $V_1$ by intersecting the set of target vertices of the
edges in $O_v$ and the set $V$ of all vertices in the loaded
partitions (Line~\ref{l:intersect1}). $V_1$ thus contains the vertices
whose edge lists need to be merged with $v$'s edge list. If an
out-neighbor of $v$ is not in $V$, we skip it in the current superstep
--- this vertex will be processed later in a future superstep in which
its partition is loaded together with $v$'s partition.

Next, we add $O_v$ into a list $\mathit{listsToMerge}$ together with
$D_u$ of each vertex $u$ in $V_1$ (Line~\ref{l:createlists} --
\ref{l:createlistsend}), and merge these lists into a new sorted list
(Line~\ref{l:firstend}).  Since all input lists are already sorted,
function \textsc{MatchAndMergeSortedArrays} can be efficiently
implemented by repeatedly finding the minimum (using an
O($\mathit{log}|V|$) min-heap algorithm~\cite{minheap}) among the
elements in a slice of the input lists and copying it into the output
array. This whole algorithm has an O($|E|\mathit{log}|V|$) complexity,
which is more efficient, both theoretically and empirically, than
scanning edges individually (O($|E|^2$)) because $|V|$ is much smaller
than $|E|$.  Furthermore, edge duplicate checking can be automatically done
during the merging --- if multiple elements have the same minimum
value, only one is copied into the output array. Label matching is
performed before copying --- an edge is not copied into the output if
it has an inconsistent label.

Line~\ref{l:secondstart} -- \ref{l:secondend} perform the same logic
by computing a new set of vertices $V_2$, and merging $D_v$ and all
the edges (\ie, $O_u \cup D_u$) of each vertex $u \in V_2$.  At
Line~\ref{l:secondend}, all the new edges to be added to vertex $v$
are in $\mathit{mergeResult}$.  Finally, to prepare for the next
iteration, $O_v$ and $D_v$ are merged (Line~\ref{l:finalmerge}) to
form the new $O_v$; $D_v$ is then updated to contain the newly added
edges (excluding those that already exist in $O_v$).

\MyPara{Example} Figure~\ref{fig:example}(c) shows the in-memory edge
lists at the end of the first iteration of the loop at
Line~\ref{l:loopstart} in Algorithm~\ref{alg:ep}. In the next
iteration, thread $t_0$ would merge $O_0$ with $D_1$ and $D_4$, and
$D_0$ with $O_2\cup D_2$, $O_3\cup D_3$, and $O_5 \cup D_5$. $O_0$ and
$O_1$ (and $O_4$) do not need to be merged again as this has been done
before.

Another advantage of this algorithm is that it runs completely in
parallel without needing any synchronization. While the edge list of a
vertex may be \emph{read} by different threads, edge addition can only
be done by one single thread, that is, the one that processes the
vertex.

%Although we load two partitions at a time, our computation does not
%depend on the number $n$ of partitions to be loaded in a superstep
%because these $n$ partitions need to be merged anyways before
%computation starts. However, loading 1 partition does not work as we
%would miss cross-partition opportunities.  We choose 2 partitions in
%each superstep because 2 is the smallest possible $n$ and thus needs
%minimum random disk accesses.

 
\subsection{Postprocessing} 
When a superstep is done, the updated edge lists need to be written
back to their partition files.  In addition, the degree file is
updated with the new vertex degree information. The (in-memory) DDM
needs to be updated with the new edge distribution information. 
%Since
%the destination of a new edge can be in any vertex interval, we
%perform an incremental update of the DDM upon an edge
%addition. Besides the percentage, we let each cell of the DDM also
%record the absolute number of edges going from one partition to
%another. This absolute number is bumped up every time an edge is added
%and the percentage is recomputed at the end of each superstep.

\MyPara{Repartitioning} If the size of a partition exceeds a threshold
(\eg, a parameter), repartitioning occurs. It is easy for Graspan to
repartition an oversized partition since the edge lists are
sorted. Graspan breaks the original vertex interval into two small
intervals, and edges are automatically restructured. The goal is to
have the two small vertex intervals to have similar numbers of edges,
so that the resulting partitions have similar sizes.  The VIT needs to
be updated with the new interval information. Repartitioning can also
be triggered in the middle of a superstep if too many edges are added
in the superstep and the size of the loaded partitions is close to the
memory size.

\MyPara{Scheduling} When a new superstep starts, two new partitions
will be selected by the scheduler to join. Since a
partition on which the computation was done in the previous superstep
may be chosen again, Graspan delays the writing of a partition back to
disk until the new partitions are chosen by the scheduler. If a chosen
partition is already in memory, significant amounts of disk I/O can be
saved.

We have developed a novel scheduling algorithm that has two
objectives: (1) maximize the number of edge pairs that can potentially
match and (2) favor the reuse of in-memory partitions. For (1), the
scheduler consults the DDM.  While the percentage information recorded
in the DDM measures the matching opportunities between two partitions,
it is an overall measurement that does not reflect the changes. Hence,
we add another field to each cell of the DDM that records, for a pair
of partitions $p$ and $q$, the change in the percentage of the edges
going from $p$ into $q$ since the last time $p$ and $q$ are both
loaded.  If $p$ and $q$ have never been loaded together, the change is
the same as the full percentage.

Using $\delta(p, q)$ to denote this change, our scheduler selects a
pair of partitions that have the largest $\delta(p, q) + \delta(q,
p)$ score. If multiple pairs of partitions have similar scores (\eg,
in a user-defined range), Graspan picks one that involves an in-memory
partition. These delta fields in the DDM also determine the termination
of the computation --- for $p$ and $q$ whose $\delta(p, q) + \delta(q,
p)$ is zero, no computation needs to be done on them. Graspan
terminates when the delta field in every single cell of the DDM
becomes 0.

\MyPara{Reporting Results} Graspan provides an API for the user to
iterate over edges with a certain label. For example, for the pointer
analysis, edges with the $\mathit{OF}$ label indicate a points-to
solution, while edges with the $\mathit{MA}$ and $\mathit{VA}$ label
represent aliasing variables. Graspan also provides translation APIs
that can be used to map vertices and edges back to variables and
statements in the program.
